/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.api_spark_kaiapp;
import api.APIKaiapp;
import com.formdev.flatlaf.FlatLightLaf;
import view.MenuManagement;

/**
 *
 * @author DELL LATTITUDE 7400
 */
//sebagai controller
public class API_SPARK_KAIAPP {

    public static void main(String[] args) {
        
        //set flatlight
        FlatLightLaf.install();
        
        // Memanggil metode untuk menginisialisasi operasi CRUD
        APIKaiapp api = new APIKaiapp();
        api.apikai();
        
        //menuju form main login
        MenuManagement menu = new MenuManagement();
        menu.setVisible(true);
    }
}
