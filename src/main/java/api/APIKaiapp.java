/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package api;

import com.google.gson.Gson;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import model.koneksi.Akun;
import model.koneksi.DatabaseConnection;
import spark.Spark;

/**
 *
 * @author DELL LATTITUDE 7400
 */
public class APIKaiapp {
    
    public static void apikai(){

        // Menjalankan aplikasi Spark Java sebagai webservice
        // meneruskan login
        Spark.get("/users/:username/:password", (req, res) -> {
            String user1 = req.params("username");
            String pass1 = req.params("password");

            // Kueri ke database untuk mendapatkan ID berdasarkan username dan password
            try (Connection connection = DatabaseConnection.getConnection()) {
                String query = "SELECT id_biodata FROM biodata WHERE username = ? AND password = ?";
                PreparedStatement statement = connection.prepareStatement(query);
                statement.setString(1, user1);
                statement.setString(2, pass1);
                ResultSet resultSet = statement.executeQuery();

                if (resultSet.next()) {
                    int id = resultSet.getInt("id_biodata");
                    return id;
                } else {
                    int id = 0;
                    return "User not found"+id;
                    
                }
            } catch (SQLException e) {
                e.printStackTrace();
                return "Error retrieving user ID"+e;
            } 
        });


        // meneruskan registrasi
        Spark.post("/biodata", (req, res) -> {
            
        // Ambil tubuh permintaan sebagai String JSON
        String requestBody = req.body();
    
        // Gunakan Gson untuk mengonversi String JSON menjadi objek Java
        Gson gson = new Gson();
        Akun data = gson.fromJson(requestBody, Akun.class);

        // Mengakses data dari objek Java yang dihasilkan
        String nik = data.getNik();
        String nama = data.getNama();
        String username = data.getUsername();
        String pass = data.getPassword();
        String telp = data.getNoTelp();

            // Melakukan operasi yang sesuai, misalnya menyimpan data ke database
            try (Connection connection = DatabaseConnection.getConnection()) {
                System.out.println("Membuka koneksi ke database...");
                System.out.println("Koneksi ke database berhasil dibuka.");

                String insertQuery = "INSERT INTO biodata (nik, nama, username, password, telepon) VALUES (?, ?, ?, ?, ?)";
                PreparedStatement preparedStatement = connection.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS);
                preparedStatement.setString(1, nik);
                preparedStatement.setString(2, nama);
                preparedStatement.setString(3, username);
                preparedStatement.setString(4, pass);
                preparedStatement.setString(5, telp);
                int rowsAffected = preparedStatement.executeUpdate();

                if (rowsAffected > 0) {
                    // Mengambil ID yang dihasilkan oleh operasi INSERT
                    ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
                    if (generatedKeys.next()) {
                        int userId = generatedKeys.getInt(1);
                        System.out.println("Data user dengan ID " + userId + " berhasil ditambahkan!");
                        return "Data user dengan ID " + userId + " berhasil ditambahkan!";
                    }
                }

                System.out.println("Gagal menambahkan data user!");
                return "Gagal menambahkan data user!";
            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("Terjadi kesalahan dalam penyimpanan data!");
                return "Terjadi kesalahan dalam penyimpanan data!"+e;
            } 
        });
        
    }
    
}
